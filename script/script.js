let Load = () => {
    try{
        let red = document.getElementById("red").value;
        let green = document.getElementById("green").value;
        let blue = document.getElementById("blue").value;
        let total = parseInt(red) + parseInt(green) + parseInt(blue);

        document.body.style.backgroundColor ='rgb('+red+','+green+','+blue+')';
        if(total <= 200) {
            document.getElementById("container").style.color = 'rgb(255,255,255)';
        }
        else if(total > 200) {
            document.getElementById("container").style.color = 'rgb(0,0,0)';
        }
    }
    catch(e){
        alert(e.message);
    }
}

let Reset = () =>{
    try{
        document.body.style.backgroundColor = 'rgb(255,255,255)';
        document.getElementById("red").value = 255;
        document.getElementById("redtxt").value=255;
        document.getElementById("green").value = 255;
        document.getElementById("greentxt").value=255;
        document.getElementById("blue").value = 255;
        document.getElementById("bluetxt").value=255;
        document.getElementById("container").style.color = 'rgb(0,0,0)'
    }
    catch(e){
        alert(e.message);
    }
}

let Change = (slide, txt) =>{
    document.getElementById(txt).value = document.getElementById(slide).value;
}


let TxtChange = (txt, slide) =>{
    let redtxt = document.getElementById(''+txt+'').value;
    if(parseInt(redtxt) || redtxt == ''){
        if(redtxt < 256 && redtxt >= 0){
            document.getElementById(''+slide+'').value =  redtxt;
        }
         else{
            redtxt = 255;
    }
    }
    else{
        redtxt = 255;
    }
        document.getElementById(''+txt+'').value = redtxt;
        document.getElementById(''+txt+'').innerHTML = redtxt;
        document.getElementById(''+slide+'').value =  redtxt;

}




