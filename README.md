# Best Practices for GUI design #

Johnathon Proffitt

### Be Simple ###
The Best user interface is no user interface. Ideally the application would do exactly what the user
wants without input from the user. For example, the clock on your PC. It used to have to be set
manually, but was changed to automatically go online and retrieve the correct time.
Where this isn’t possible, less is more. You want the essential common cases to be at the front,
clearly accessible and without distractions.

Reference: [The Importance of Simplicity](https://msdn.microsoft.com/en-us/library/ms993297.aspx)

### Be Clear ###
This is the most important element of a User Interface. The user should know exactly how your
application works, and what the applications options do.

Reference: [Principles of User Interface Design](http://bokardo.com/principles-of-user-interface-design/)  
Reference: [8 Characteristics of Successful User Interfaces](http://usabilitypost.com/2009/04/15/8-characteristics-of-successful-user-interfaces/)  

### Be Familiar ###
A familiar interface is an intuitive interface. You can make an interface familiar by using common UI
elements, or following common practices in other successful applications of similar types.

Reference: [8 Characteristics of Successful User Interfaces](http://usabilitypost.com/2009/04/15/8-characteristics-of-successful-user-interfaces/)  

### Be Predictable ###
The User should know what to expect when they perform an action. They should know what the
application will do whenever they interact with it.

Reference: [Predictability: 5 Principles of Interaction Design To Supercharge Your UI (4 of 5)](http://www.givegoodux.com/predictability-5-principles-of-interaction-design-to-supercharge-your-ui-4-of-5/)

### Be Consistent ###
It is important that the design remain the same across the application. This allows the interface to
feel familiar, and be predictable. Use the same fonts, and colour palette throughout your project.

Reference: [5 aspects of a good user interface](http://www.argondesign.com/news/2014/feb/5/5-aspects-good-user-interface/)  
Reference: [8 Characteristics of Successful User Interfaces](http://usabilitypost.com/2009/04/15/8-characteristics-of-successful-user-interfaces/)  

### Be Communicative ###
Always let the user know where they are in the application, what they can do, what has changed,
and any errors. Let the user know the current status of the application, and let them know what to
do next.

Reference: [8 Characteristics of Successful User Interfaces](http://usabilitypost.com/2009/04/15/8-characteristics-of-successful-user-interfaces/)  
Reference: [10 Rules of Good UI Design](https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project)  

### Be Attractive ###
Think about the placement of elements, the use of white space, and the colour palette your
application uses, and how they all interact with each other. Consider the psychology of colours in
choosing palette. You want the user to enjoy looking at your application.

Reference: [5 aspects of a good user interface](http://www.argondesign.com/news/2014/feb/5/5-aspects-good-user-interface/)  
Reference: [8 Characteristics of Successful User Interfaces](http://usabilitypost.com/2009/04/15/8-characteristics-of-successful-user-interfaces/)  

### Be Responsive ###
The user won't enjoy an application where the interface is lagging, and struggling to keep up with
them. Whenever possible, the interface should flow with the user. Where this isn't possible, a
loading notification, or some actively updating information should keep stop the user from feeling
disconnected.

Reference: [5 aspects of a good user interface](http://www.argondesign.com/news/2014/feb/5/5-aspects-good-user-interface/)  
Reference: [8 Characteristics of Successful User Interfaces](http://usabilitypost.com/2009/04/15/8-characteristics-of-successful-user-interfaces/)  


### Peer Review ###
Matt Collecutt
The report is very descriptive, highlighting good points towards an efficient and effective GUI.





